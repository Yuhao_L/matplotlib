import matplotlib.pyplot as plt
import numpy as np
import tkinter as tk

def read_scores():
    """This function will read a file of names and scores and return a
    dictionary containing the information with the name as the key"""
    fopen = open("grades.txt")
    fread=fopen.readlines()
    scores_list=[]
    for i in fread:
        temp = i.split(',')
        scores_list.append(temp)
    fopen.close()
    Output={}
    for elem in scores_list:
        temp=[]
        name=elem[0]
        for i in elem:
            if i!=name:
                temp.append(int(i))
        Output[name]=temp

    return Output
        
def get_single_grade(score, grade_no):
    """This function will return a list of one grade for all students"""
    score_list=[] 
    for key,value in score.items():
         score_list.append(value[grade_no-1])#grade is in index 1 less than grade number
    return score_list
                    
def get_names(score):
    """This function will return a list of the names of students"""
    names=[]
    for key,value in score.items():
        names.append(key)
        names.sort()
    return names

def bar_graph(labels,grades,choice):
    """This function produces a bar graph with one data set"""
    x=np.arange(len(labels))
    width = 0.5

    fig,ax = plt.subplots()
    rects1=ax.bar(x-width+.5,grades,width,align='center')
    ax.set_ylabel('Grade')
    ax.set_xticks(x)
    ax.set_xticklabels(labels, fontsize=8)
    autolabel(ax,rects1)
    fig.tight_layout()
    ax.set_title("Assignment " + str(choice))
    plt.show()

def multiple_bar(score_dict, labels):
    """This function creates a bar graph that displays a separate bar for each assignment"""
    scores=get_list_scores(score_dict)
    fig,ax=plt.subplots()
    n=len(labels)
    gap=1/n
    ind = np.arange(n)
    
    p1=ax.bar(ind-2*gap,scores[0],width=gap, color='red')
    p2=ax.bar(ind-gap,scores[1],width=gap, color='green')
    p3=ax.bar(ind,scores[2],width=gap, color='yellow')
    p4=ax.bar(ind+gap,scores[3],width=gap,color='blue')
    ax.set_xticks(ind)
    ax.set_xticklabels(labels, fontsize=8)
    ax.legend((p1[0],p2[0],p3[0],p4[0]),('Assign 1','Assign 2','Assign 3','Assign 4'))
    autolabel(ax,p1)
    autolabel(ax,p2)
    autolabel(ax,p3)
    autolabel(ax,p4)
    ax.set_title("All Assignment")
    
    
    fig.tight_layout()
    plt.show()
    
def histogram(grades,choice):
    """function creates bar graph for one assignment"""
    bin_no=[0,10,20,30,40,50,60,70,80,90,100]
    
    fig,ax = plt.subplots()
    counts, bins, patches = ax.hist(grades,bins=bin_no, facecolor='yellow', edgecolor='gray')
    ax.set_title("Assignment " + str(choice))
    ax.set_xticks(bins)
    ax.set_xlabel('Grade')
    ax.set_ylabel('Number of Students')
    
    #code to put numbers at bottom of each bar
    bin_centers=0.5*np.diff(bins)+bins[:-1]
    for count, x in zip(counts,bin_centers):
        ax.annotate(str(count),xy=(x,0), xycoords=('data','axes fraction'),xytext=(0,-18), textcoords='offset points', va='top',ha='center')

    plt.subplots_adjust(bottom=0.15) #gives extra space at bottom   
    plt.show()

def multiple_hist(score_dict, his_type):
    """function creates a histogram with all four assignments"""
    scores=get_list_scores(score_dict)

    bin_no=[0,10,20,30,40,50,60,70,80,90,100]
    fig,ax = plt.subplots()
    ax.set_title("All Assignments")
    label = ['Assign 1', 'Assign 2', 'Assign 3', 'Assign 4']
    ax.hist(scores, bins=bin_no, edgecolor='navy',histtype=his_type, label=label)
    ax.set_xticks(bin_no)
    ax.legend(prop={'size': 10})
    ax.set_xlabel('Grade')
    ax.set_ylabel('Number of Students')
    
    plt.show()

def frequency(score):
    """This function will divide a list of scores into the number of scores in each letter grade.  It will return
        a list [F,D,C,B,A]"""
    a=0
    b=0
    c=0
    d=0
    f=0

    #divides the data into approprate letter scores
    for i in score:
        if i<60:
            f=f+1
        elif i<70:
            d=d+1
        elif i<80:
            c=c+1
        elif i<90:
            b=b+1
        else:
            a=a+1
    #creates a list with numbers of F,D,C,B,A
    frequency_list=[]
    frequency_list.append(f)
    frequency_list.append(d)
    frequency_list.append(c)
    frequency_list.append(b)
    frequency_list.append(a)
    return frequency_list

def pie_chart(score,choice):
    """creates bar graph displaing number of each letter grade on an assignment"""
    sizes=frequency(score)
    labels=['F', 'D', 'C', 'B', 'A']
    #removes any category with 0 scores
    while 0 in sizes:
        re = sizes.index(0)
        sizes.remove(0)
        del labels[re]
    colors=['red', 'orange', 'yellow', 'green', 'blue']
    fig1,ax1=plt.subplots()

    patches,texts,autotexts=ax1.pie(sizes,colors=colors, labels=labels,autopct=lambda p: '{:.1f}%'.format(round(p)) if p > 0 else '',startangle=90)

    ax1.axis('equal')
    ax1.set_title("Assignment " + str(choice))
    plt.tight_layout()
    plt.show()

def multiple_pie(score_dict):
    """This function creates a graph with a separate pie chart for each assignment"""
    scores=get_list_scores(score_dict)

    sizes1=frequency(scores[0])
    sizes2=frequency(scores[1])
    sizes3=frequency(scores[2])
    sizes4=frequency(scores[3])
    labels1=['F', 'D', 'C', 'B', 'A']
    labels2=['F', 'D', 'C', 'B', 'A']
    labels3=['F', 'D', 'C', 'B', 'A']
    labels4=['F', 'D', 'C', 'B', 'A']
    while 0 in sizes1:
        re = sizes1.index(0)
        sizes1.remove(0)
        del labels1[re]
    while 0 in sizes2:
        re = sizes2.index(0)
        sizes2.remove(0)
        del labels2[re]
    while 0 in sizes3:
        re = sizes3.index(0)
        sizes3.remove(0)
        del labels3[re]
    while 0 in sizes4:
        re = sizes4.index(0)
        sizes4.remove(0)
        del labels4[re]
    colors=['red', 'orange', 'yellow', 'green', 'blue']
    
    fig,axs=plt.subplots(2,2)
    fig.suptitle('Pie Charts for Each Assignment')

    patches,texts,autotexts=axs[0,0].pie(sizes1,colors=colors, labels=labels1,autopct=lambda p: '{:.1f}%'.format(round(p)) if p > 0 else '',startangle=90)
    patches,texts,autotexts=axs[0,1].pie(sizes2,colors=colors, labels=labels2,autopct=lambda p: '{:.1f}%'.format(round(p)) if p > 0 else '',startangle=90)
    patches,texts,autotexts=axs[1,0].pie(sizes3,colors=colors, labels=labels3,autopct=lambda p: '{:.1f}%'.format(round(p)) if p > 0 else '',startangle=90)
    patches,texts,autotexts=axs[1,1].pie(sizes4,colors=colors, labels=labels4,autopct=lambda p: '{:.1f}%'.format(round(p)) if p > 0 else '',startangle=90)

    axs[0,0].set_title('Assignment 1')
    axs[0,1].set_title('Assignment 2')
    axs[1,0].set_title('Assignment 3')
    axs[1,1].set_title('Assignment 4')
    plt.show()

def get_list_scores(score_dict):
    """This function returns a list of the scores for each of the four assignments"""
    score_list=[]
    i=1
    while i<5:
        assign=get_single_grade(score_dict,i)
        score_list.append(assign)
        i=i+1
    return score_list

def autolabel(ax,bars):
    """This function labels the bar graph with the number in each bar"""
    for i in bars:
        height=i.get_height()
        ax.annotate('{}'.format(height),
                    xy=(i.get_x()+i.get_width()/2,height),
                    xytext=(0,3),
                    textcoords="offset points",ha='center',va='bottom')



def box_and_whisker(grades, choice):

    fig,ax=plt.subplots()
    bp=ax.boxplot(grades)
    ax.set_title("Assignment " + str(choice))
    plt.show()
    


def multiple_box(score_dict):
    grades=get_list_scores(score_dict)
    fig,ax=plt.subplots()
    bp=ax.boxplot(grades)
    ax.set_title("All Assignments")
    plt.xticks([1,2,3,4],['Assign 1', 'Assign 2', 'Assign 3', 'Assign 4'])
    
    plt.show()

def close_window(window):
    window.destroy()

def bar_choice():
    choice=tk.Tk(className="Assignment Choice")
    choice_label=tk.Label(choice,text="What assignment would you like to see in your bar graph?",fg="red", width=50,height=3)
    choice_label.pack()

    button1=tk.Button(choice, text="Assignment 1", bg="yellow", fg="black", command=lambda: bar_graph(labels,get_single_grade(score_dict,1),1))
    button1.pack()
    button2=tk.Button(choice, text="Assignment 2", bg="green", fg="black", command=lambda: bar_graph(labels,get_single_grade(score_dict,2),2))
    button2.pack()
    button3=tk.Button(choice, text="Assignment 3", bg="blue", fg="black", command=lambda: bar_graph(labels,get_single_grade(score_dict,3),3))
    button3.pack()
    button4=tk.Button(choice, text="Assignment 4", bg="red", fg="black", command=lambda: bar_graph(labels,get_single_grade(score_dict,4),4))
    button4.pack()
    button5=tk.Button(choice, text="All Assignement on one Graph", bg="orange", fg="black", command=lambda: multiple_bar(score_dict, labels))
    button5.pack()
    button6=tk.Button(choice, text="Go Back",bg="pink", fg="black",command=lambda: close_window(choice))
    button6.pack()

def pie_choice():
    pchoice=tk.Tk(className="Assignment Choice")
    pchoice_label=tk.Label(pchoice,text="What assignment would you like to see in your pie chart?",fg= "red", width=50,height=3)
    pchoice_label.pack()
    button1=tk.Button(pchoice, text="Assignment 1", bg="yellow", fg="black", command=lambda: pie_chart(get_single_grade(score_dict,1),1))
    button1.pack()
    button2=tk.Button(pchoice, text="Assignment 2", bg="green", fg="black", command=lambda: pie_chart(get_single_grade(score_dict,2),2))
    button2.pack()
    button3=tk.Button(pchoice, text="Assignment 3", bg="blue", fg="black", command=lambda: pie_chart(get_single_grade(score_dict,3),3))
    button3.pack()
    button4=tk.Button(pchoice, text="Assignment 4", bg="red", fg="black", command=lambda: pie_chart(get_single_grade(score_dict,4),4))
    button4.pack()
    button5=tk.Button(pchoice, text="All four Assignments", bg="orange", fg="black", command=lambda: multiple_pie(score_dict))
    button5.pack()
    button6=tk.Button(pchoice, text="Go Back",bg="pink", fg="black",command=lambda: close_window(pchoice))
    button6.pack()
    

def hist_choice():
    hchoice=tk.Tk(className="Assignment Choice")
    hchoice_label=tk.Label(hchoice,text="What assignment would you like to see in your histogram?",fg= "red", width=50,height=3)
    hchoice_label.pack()
    button1=tk.Button(hchoice, text="Assignment 1", bg="yellow", fg="black", command=lambda: histogram(get_single_grade(score_dict,1),1))
    button1.pack()
    button2=tk.Button(hchoice, text="Assignment 2", bg="green", fg="black", command=lambda: histogram(get_single_grade(score_dict,2),2))
    button2.pack()
    button3=tk.Button(hchoice, text="Assignment 3", bg="blue", fg="black", command=lambda: histogram(get_single_grade(score_dict,3),3))
    button3.pack()
    button4=tk.Button(hchoice, text="Assignment 4", bg="red", fg="black", command=lambda: histogram(get_single_grade(score_dict,4),4))
    button4.pack()
    button5=tk.Button(hchoice, text="All four Assignments Stacked", bg="orange", fg="black", command=lambda: multiple_hist(score_dict, 'barstacked'))
    button5.pack()
    button7=tk.Button(hchoice, text="All four Assignments Side by Side", bg="cyan", fg="black", command=lambda: multiple_hist(score_dict,'bar'))
    button7.pack()
    button6=tk.Button(hchoice, text="Go Back",bg="pink", fg="black",command=lambda: close_window(hchoice))
    button6.pack()


def box_choice():
    bchoice=tk.Tk(className="Assignment Choice")
    bchoice_label=tk.Label(bchoice,text="What assignment would you like to see in your box and whisker plot?",fg= "red", width=60,height=3)
    bchoice_label.pack()
    button1=tk.Button(bchoice, text="Assignment 1", bg="yellow", fg="black", command=lambda: box_and_whisker(get_single_grade(score_dict,1),1))
    button1.pack()
    button2=tk.Button(bchoice, text="Assignment 2", bg="green", fg="black", command=lambda: box_and_whisker(get_single_grade(score_dict,2),2))
    button2.pack()
    button3=tk.Button(bchoice, text="Assignment 3", bg="blue", fg="black", command=lambda: box_and_whisker(get_single_grade(score_dict,3),3))
    button3.pack()
    button4=tk.Button(bchoice, text="Assignment 4", bg="red", fg="black", command=lambda: box_and_whisker(get_single_grade(score_dict,4),4))
    button4.pack()
    button5=tk.Button(bchoice, text="All four Assignments", bg="orange", fg="black", command=lambda: multiple_box(score_dict))
    button5.pack()
    button6=tk.Button(bchoice, text="Go Back",bg="pink", fg="black",command=lambda: close_window(bchoice))
    button6.pack()



    
def window_choice():
    """This function creates the first choice window and calls the correct method based on the choice"""
    window = tk.Tk(className="Graphing Data")
    label1=tk.Label(window,text="What type of graph would you like to see?",fg="red", width=50, height=3)
    label1.pack()
    button1=tk.Button(window, text="Bar Graph", bg="yellow", fg="black", command=lambda: bar_choice())
    button1.pack()
    button2=tk.Button(window, text="Pie Chart", bg="red", fg="black", command=lambda: pie_choice())
    button2.pack()
    button3=tk.Button(window, text="Histogram", bg="blue", fg="black", command=lambda: hist_choice())
    button3.pack()
    button4=tk.Button(window, text="Box and Whisker", bg="green", fg="black", command=lambda: box_choice())
    button4.pack()
    button5=tk.Button(window, text="Quit",bg="pink", fg="black",command=lambda: close_window(window))
    button5.pack()
    window.mainloop()

    
score_dict=read_scores()
labels=get_names(score_dict)
window_choice()